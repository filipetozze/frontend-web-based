<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Pessoas;
use App\Http\Controllers\Controller;

use Exception;
use DB;
use Log;


class PessoasController extends Controller
{


	 public function list( Request $request )
    {
      $ObDados = Pessoas::all();
      return $ObDados;
  
    }

    public function listId($id)
    {
      $ObDados = Pessoas::find($id);;
      return $ObDados;
  
    }

    public function insert(Request $request)
    {

      $dados = $request->all();
      $insert = new Pessoas;
      $insert->nome = $dados['nome'];
      $insert->dt_nascimento = $dados['dtNascimento'];
      $insert->doc_identificacao = $dados['docIndentificacao'];
      $insert->sexo = $dados['sexo'];
      $insert->endereco = $dados['endereco'];
      $insert->save();
      
    }

    public function update( Request $request )
    {
      $dados = $request->all();
      $update = Pessoas::find($dados['id']);
      $update->nome = $dados['nome'];
      $update->dt_nascimento = $dados['dtNascimento'];
      $update->doc_identificacao = $dados['docIndentificacao'];
      $update->sexo = $dados['sexo'];
      $update->endereco = $dados['endereco'];
      $update->save();
      
    }

    public function delete($id)
    {
      $delete = Pessoas::find($id);
      $delete->delete();
      
    }

}