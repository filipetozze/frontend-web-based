<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/pessoas', function (Request $request) {
//     return $request->user();
// });

Route::middleware(['api'])->group(function () {
    Route::get('/pessoas', 'PessoasController@list');
    Route::get('/pessoas/{id}', 'PessoasController@listId');
	Route::post('/pessoas', 'PessoasController@insert');
	Route::put('/pessoas', 'PessoasController@update');
	Route::delete('/pessoas/{id}', 'PessoasController@delete');
});

