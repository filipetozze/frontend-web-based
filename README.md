# **NEPPO - Teste Frontend Web Based** #


#Instalação e execução:
O projeto está separado em duas partes: back-end e front-end.

	Front-end
		-> Instale o quasar cli usando o comando 'npm install -g quasar-cli'
		-> Navegar até a pasta front-end/ e usar o camando 'npm install' para baixar as dependências.
		-> Após baixar os pacotes, utilizar o comando 'quasar dev' para gerar um build no modo desenvolvimento e irá abri na url local http://localhost:8080/#/pessoas
		-> Para criar um build dos arquivos (processados e minificados) para um servidor em produção, basta para o servidor levantado no front-end e usar o  comando 'quasar build' onde será criado uma pasta na raiz do front chamada 'dist'
		
	Back-end
		-> Navegar até a pasta back-end e usar o compando 'composer install' para baixar as dependências
		-> Após baixar os pacotes, para levantar um servidor local, utilize o comando 'php artisan serve' e o mesmo será aberto na url local http://127.0.0.1:8000 (url configurada no front-end, caso mudar a porta irá dar falha na conexão entre front e back)
		-> Rotas com os verbos HTTP(GET, POST, PUT, DELETE) solicitados se encontra em back-end/routes/api.php
		-> Métodos do CRUD se encontra em back-end/app/Http/Controllers/PessoasController.php
		-> Banco de dados SqLite se encontra em back-end/database/db.db


#Tecnologias usadas
-> Quasar Framework (http://v0-14.quasar-framework.org/)

-> Laravel (https://laravel.com/)

-> VueJs (https://vuejs.org/)

-> Composer (https://getcomposer.org/download/)

-> NodeJs(para usar o npm) (https://nodejs.org/en/download/)

-> Material Desing (css) - integrado com quasar framework

-> SqLite

